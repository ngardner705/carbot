/********
 * CarBot
 * @ngardner
 *
 */

$(document).ready(function() {
    
    var wsCarBot;
    var signalStrength = '0%';
    var throttlePosition = 0;
    var steeringPosition = 100;
    var servoPosition = 5;
    var ForR = 'F';
    
    /** CREATE **/
    // connection to webservice
    wsCarBot = new CarBotClient();
    wsCarBot.init();
    
    /** CONNECTED EVENT **/
    document.addEventListener('carbot-connected',function() {
      $('#carbot-loading').hide();
      $('#carbot-connected').show();
      $('#status').show();
    });
    
    /** ERROR EVENT **/
    document.addEventListener('carbot-error',function(msgData) {
      console.log(msgData);
      $('#intro').show();
      $('#intro #carbot-connected').hide();
      $('#intro #carbot-loading').html("<h2>ERROR</h2><p>"+msgData.detail+"</p>").show();
    });
    
    /** STARTED EVENT **/
    document.addEventListener('carbot-start',function(msgData) {
      $('#intro').slideUp();
      $('#carbotInterface').show();
      $('#finish').show();
    });
    
    /** STATUS EVENT **/
    document.addEventListener('carbot-status',function(msgData) {
      signalStrength = (Math.round(eval(msgData.detail.signalStrength)*100)+'%');
      $('#status .status-wifi').html(signalStrength);
    });
    
    /** FINISH EVENT **/
    document.addEventListener('carbot-finish',function(msgData) {
      $('#intro').slideDown();
      $('#carbotInterface').hide();
      $('#finish').hide();
    });
    
    /** Hookup UI to WebSocket **/
    interact('.slider-horizontal .slider')                   // target the matches of that selector
      .origin('self')                     // (0, 0) will be the element's top-left
      .restrict({drag: 'self'})           // keep the drag within the element
      .draggable({                        // make the element fire drag events
        max: Infinity                     // allow drags on multiple elements
    })
    .on('dragmove', function (event) {  // call this function on every move
      
      var sliderWidth = interact.getElementRect(event.target.parentNode).width;
      var value = event.pageX / sliderWidth;
      event.target.setAttribute('data-value', value.toFixed(2));
      
      // fire events
      if(event.target.classList.contains('control-steering')) {
        steeringPosition = value * 200;
        wsCarBot.carbotControl(throttlePosition,steeringPosition);
      }
      if(event.target.classList.contains('control-servo')) {
        value = (Math.round(value * 10) / 10).toFixed(2);
        servoPosition = value * 10;
        wsCarBot.carbotServo(servoPosition);
      }
      
      // move the slider
      event.target.childNodes[0].style.left = (value * 100) + '%';
      
      
    })
    .on('dragend',function(event) {
      if(event.target.classList.contains('control-steering')) {
        event.target.setAttribute('data-value', 0.5);
        event.target.childNodes[0].style.left = '50%';
        steeringPosition = 100;
        wsCarBot.carbotControl(throttlePosition,steeringPosition);
      }
      if(event.target.classList.contains('control-servo')) {
        //wsCarBot.carbotServostop();
        /*event.target.setAttribute('data-value', 0.5);
        event.target.childNodes[0].style.left = '50%';
        servoPosition = 5;
        wsCarBot.carbotServo(servoPosition);*/
      }
    });
    
    interact('.slider-vertical .slider')                   // target the matches of that selector
      .origin('self')                     // (0, 0) will be the element's top-left
      .restrict({drag: 'self'})           // keep the drag within the element
      .draggable({                        // make the element fire drag events
        max: Infinity                     // allow drags on multiple elements
    })
    .on('dragmove', function (event) {  // call this function on every move
      var sliderHeight = interact.getElementRect(event.target.parentNode).height;
      var value = event.pageY / sliderHeight;
      event.target.setAttribute('data-value', value.toFixed(2));
      
      // move the slider
      event.target.childNodes[0].style.bottom = (100 - (value * 100)) + '%';
      
      // fire events
      if(event.target.classList.contains('control-throttle')) {
        if(ForR == 'R') {
          value = (100 - (value * 100))*-1;
        } else {
          value = (100 - (value * 100));
        }
        throttlePosition = value;
        wsCarBot.carbotControl(throttlePosition,steeringPosition);
      }
    })
    .on('dragend',function(event) {
      event.target.setAttribute('data-value', 0);
      event.target.childNodes[0].style.bottom = '0%';
      
      if(event.target.classList.contains('control-throttle')) {
        throttlePosition = 0;
        wsCarBot.carbotControl(throttlePosition,steeringPosition);
      }
      
    });
    
    interact.maxInteractions(Infinity);
    
    $('#start').click(function() {
      wsCarBot.carbotStart();
    });
    
    $('#finish').click(function() {
      wsCarBot.carbotControl(0,100);
      wsCarBot.carbotFinish();
    });
    
    $('#carbot-direction').click(function() {
      var isReverse = $(this).hasClass('active');
      if(isReverse) {
        direction = 'reverse';
        $(this).html('R');
        ForR = 'R';
      } else {
        direction = 'forward';
        $(this).html('F');
        ForR = 'F';
      }
    });

var canvas = document.createElement("canvas");
$('#video').append(canvas);

var videoUri = "ws://192.168.1.41:8080";
var wsavc = new WSAvcPlayer(canvas,"webgl",1,35);
wsavc.connect(videoUri);
setTimeout(function() {
    console.log('starting stream...');
    wsavc.playStream();
    console.log('playing stream');
    console.log(wsavc);
},2000);

});
