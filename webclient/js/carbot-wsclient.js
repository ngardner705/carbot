/****************
 *
 * CarBot WS Client
 * By: Nathan Gardner <nathan@factory8.com>
 * @ngardner
 *
 ***************/

function CarBotClient() {
  this.wsClient = 'something';
  this.woData = {};
  _self = this;
}

CarBotClient.prototype.carbotConnected = function(msgData) {
  
  // Create the event
  var event = new CustomEvent("carbot-connected", { "detail": msgData });
  
  // Dispatch/Trigger/Fire the event
  document.dispatchEvent(event);
  
};

CarBotClient.prototype.carbotStarted = function(msgData) {
  
  // Create the event
  var event = new CustomEvent("carbot-start", { "detail": msgData });
  
  // Dispatch/Trigger/Fire the event
  document.dispatchEvent(event);
  
};

CarBotClient.prototype.carbotStatus = function(msgData) {
  
  // Create the event
  var event = new CustomEvent("carbot-status", { "detail": msgData });
  
  // Dispatch/Trigger/Fire the event
  document.dispatchEvent(event);
  
};

CarBotClient.prototype.carbotFinished = function(msgData) {
  
  // Create the event
  var event = new CustomEvent("carbot-finish", { "detail": msgData });
  
  // Dispatch/Trigger/Fire the event
  document.dispatchEvent(event);
  
};

CarBotClient.prototype.receivedMessage = function(msgData) {
  
  if(msgData.result == 'success') {
    if(msgData.event == 'start') {
      _self.carbotStarted(msgData.response);
    } else if(msgData.event == 'finish') {
      _self.carbotFinished(msgData.response);
    } else if(msgData.event == 'open') {
      _self.carbotConnected(msgData.response);
    } else if(msgData.event == 'status') {
      _self.carbotStatus(msgData.response);
    }
  }
  
};

CarBotClient.prototype.init = function(settings) {
  
  // how to override settings?
  settings = {
    port: 9333,
    ip: '192.168.1.41'
  };
  
  // connect
  var host = "ws://"+settings.ip+":"+settings.port; // No need to change this if using localhost
  try {
    this.wsClient = new WebSocket(host);
    this.wsClient.onopen = function(msg) {
      msg = msg;
      _self.carbotConnected(msg);
    };
    this.wsClient.onmessage = function(msg) {
      if (typeof msg.data !== 'undefined') {
        var msgData = JSON.parse(msg.data);
        _self.receivedMessage(msgData);
      }
    };
    this.wsClient.onclose = function(msg) {
      return msg;
    };
    this.wsClient.onerror = function(errorEvent) {
      var event = new CustomEvent("carbot-error", { "detail": "WebSocket error" });    
      document.dispatchEvent(event);
    };
  } catch(ex) {
    console.log(ex);
  }
  
  return this;
  
};

CarBotClient.prototype.disconnect = function() {
  _self.wsClient.close();
  _self.wsClient=null;
};

CarBotClient.prototype.carbotStart = function() {
  _self.wsClient.send('**START**');
};

CarBotClient.prototype.carbotFinish = function() {
  _self.wsClient.send("**FINISH**");
};

CarBotClient.prototype.carbotControl = function(throttle,steering) {
  _self.wsClient.send("**CONTROL**" + throttle +'*'+ steering);
};

CarBotClient.prototype.carbotServo = function(servoPosition) {
  _self.wsClient.send("**SERVO**" + servoPosition);
};

CarBotClient.prototype.carbotServostop = function() {
  _self.wsClient.send("**SERVOSTOP**");
};