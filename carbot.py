#!/usr/bin/python

# Websocket stream for car controls
# using Tornado

from __future__ import division # python 2 cant math
import subprocess
import time
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
from tornado.ioloop import PeriodicCallback
import tornado.web
import RPi.GPIO as GPIO
import carbotDualdriveMotors

#Config
websocketPort = 9333 #Websocket Port 9333=WEEE
GPIO.setmode(GPIO.BCM)
statusBroadcastInterval = 5000
servoPin = 12
servoPosition = 0

#Servo setup
GPIO.setup(servoPin,GPIO.OUT)
servoDevice = GPIO.PWM(servoPin,50)
servoDevice.start(servoPosition)

#Wifi signal
#  iwconfig wlan0 | grep -i -o '[0-9][0-9]\/[0-9][0-9]'


# WebSocket Handler
class WSHandler(tornado.websocket.WebSocketHandler):

    def parseMessage(self,message):
        if message == '**START**':
            self.sendStartMessage()
            self.statusupdates.start()
        
        if message == '**FINISH**':
            self.statusupdates.stop()
            carbotDualdriveMotors.stop()
            servoDevice.stop()
            self.sendFinishMessage()
        
        if message.startswith('**CONTROL**'):
            controlInputsRaw = message[11:]
            controlInputs = controlInputsRaw.split('*')
            throttle = int(float(controlInputs[0]))
            steering = int(float(controlInputs[1]))
            carbotDualdriveMotors.driveMotorsFromInput(throttle,steering)
            self.write_message('{"result":"success","event":"control","response":"going ' + str(throttle) + ' and ' + str(steering) +'"}')
        
        if message.startswith('**SERVO**'):
            servoPosition = int(float(message[9:])) + 3
            servoDevice.ChangeDutyCycle(servoPosition)
            self.write_message('{"result":"success","event":"servo","response":"moved to ' + str(servoPosition) +'"}')
            
        if message.startswith('**SERVOSTOP**'):
            servoDevice.stop()

    def check_origin(self, origin):
        return True

    def sendStartMessage(self):
        self.write_message('{"result":"success","event":"start","response":"started up"}')

    def sendFinishMessage(self):
        self.write_message('{"result":"success","event":"finish","response":"were done"}')
        
    def sendStatusMessage(self):
        signalStrength = subprocess.Popen(["iwconfig wlan0 | grep -i -o '[0-9][0-9]\/[0-9][0-9]'"],shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT).communicate()[0].decode('utf-8')
        self.write_message('{"result":"success","event":"status","response":{"signalStrength":"'+str(signalStrength.strip())+'"}}')

    def on_message(self, message):
        self.parseMessage(message)

    def on_close(self):
        self.statusupdates.stop()
        carbotDualdriveMotors.stop()
        servoDevice.stop()
    
    def open(self):
        self.statusupdates = PeriodicCallback(self.sendStatusMessage, statusBroadcastInterval)
        self.write_message('{"result":"success","event":"open","response":"connected"}')

application = tornado.web.Application([
    (r'/', WSHandler),
])

try:  
    # here you put your main loop or block of code  
    if __name__ == "__main__":

        print("Starting")

        # setup websocket server
        websocket_server = tornado.httpserver.HTTPServer(application)
        websocket_server.listen(websocketPort)
        print("Started CarBot server on port "+str(websocketPort))
        tornado.ioloop.IOLoop.instance().start()
  
except KeyboardInterrupt:  
    # here you put any code you want to run before the program   
    # exits when you press CTRL+C  
    print("\nForce Closed CarBot") # print value of counter  
  
except:  
    # this catches ALL other exceptions including errors.  
    # You won't get any error messages for debugging  
    # so only use it once your code is working  
    print("CarBot Crashed")  
  
finally:
    ioloop = tornado.ioloop.IOLoop.instance()
    ioloop.add_callback(ioloop.stop)
    print("Stopped CarBot server")
    #GPIO.cleanup() # this ensures a clean exit 