#!/usr/bin/python

import sys
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
servoPin = 12
servoPosition = 0

#Start up servo
GPIO.setup(servoPin,GPIO.OUT)
servoDevice = GPIO.PWM(servoPin,50)
servoDevice.start(servoPosition)

print "Servo at 0"

servoDevice.ChangeDutyCycle(1)
time.sleep(3)

servoDevice.ChangeDutyCycle(11)
time.sleep(3)

servoDevice.ChangeDutyCycle(1)
time.sleep(3)

servoDevice.ChangeDutyCycle(11)
time.sleep(10)




maxPosition = 20

while servoPosition < maxPosition:
    
    print "Moving to " + str(servoPosition)
    servoDevice.ChangeDutyCycle(servoPosition)
    
    time.sleep(1)
    servoPosition += 1

print "Back and forth test"
servoDevice.ChangeDutyCycle(1)
time.sleep(1)
servoDevice.ChangeDutyCycle(12)
time.sleep(1)
servoDevice.ChangeDutyCycle(1)
time.sleep(1)
servoDevice.ChangeDutyCycle(12)
time.sleep(10)
servoDevice.ChangeDutyCycle(1)
time.sleep(10)

