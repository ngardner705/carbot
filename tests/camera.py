#!/usr/bin/python

import picamera
import time

with picamera.PiCamera() as camera:
	photoCount = 0
	camera.resolution = (640,480)
	camera.annotate_background = picamera.Color('black')
	# warm up camera
	time.sleep(2)
	camera.capture('/home/pi/carbot/tests/testimage.jpg')
