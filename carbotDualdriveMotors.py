#!/usr/bin/python

# This is for robots that have 2 drive motors, one for each wheel. Steering is done by changing speed of wheels.

from __future__ import division
from gpiozero import Motor
from time import sleep

#GPIO pins
driveMotorLF = 17 # left motor forward
driveMotorLR = 27 # left motor reverse
driveMotorRF = 22 # right motor forward
driveMotorRR = 23 # right motor reverse

#Motors
driveMotorL = Motor(driveMotorLF,driveMotorLR)
driveMotorR = Motor(driveMotorRF,driveMotorRR)

def driveMotorsFromInput(throttleInput,steerInput):
    
    #throttleInput is 0 - 100
    #steerInput is 0 - 200. 0-99 is left, 0 being full left. 100 is straight. 101-200 is right, 200 being full right.
    
    print "Throttle: " + str(throttleInput) + ", Steering: " + str(steerInput)
    
    driveMotorLSpeed = throttleInput
    driveMotorRSpeed = throttleInput
    
    if steerInput < 100:
        steerAmount = 100 - steerInput
        driveMotorRSpeed = throttleInput - steerAmount
    if steerInput > 100:
        steerAmount = steerInput - 100
        driveMotorLSpeed = throttleInput - steerAmount
        
    driveMotors(driveMotorLSpeed,driveMotorRSpeed)

def driveMotors(leftSpeed, rightSpeed):
    
    speedVarL = int(leftSpeed)/100
    speedVarR = int(rightSpeed)/100
    
    if speedVarL == 0:
        driveMotorL.stop()
    if speedVarR == 0:
        driveMotorR.stop()
    if speedVarL > 0:
        driveMotorL.forward(speedVarL)
        print "L speed = " + str(speedVarL)
    if speedVarL < 0:
        driveMotorL.backward(speedVarL*-1)
        print "L speed = " + str(speedVarL*-1) + " reverse"
    if speedVarR > 0:
        driveMotorR.forward(speedVarR)
        print "R speed = " + str(speedVarR)
    if speedVarR < 0:
        driveMotorR.backward(speedVarR*-1)
        print "R speed = " + str(speedVarR*-1) + " reverse"

def stop():
    driveMotorR.stop()
    driveMotorL.stop()
