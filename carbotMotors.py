#!/usr/bin/python

# this is for a robot thas has 1 motor for drive wheels, 1 motor for steering

from __future__ import division
from gpiozero import Motor
from time import sleep

#GPIO pins
driveMotorForward = 22
driveMotorBackward = 23
steerMotorLeft = 17
steerMotorRight = 27

#Motors
driveMotor = Motor(driveMotorForward,driveMotorBackward)
steerMotor  = Motor(steerMotorLeft,steerMotorRight)

#Motor statuses
driveStatus = 0; # <0 is reverse, 0 is stopped, 1 is full speed

def turnLeft():
    steerMotor.forward(.7)

def turnRight():
    steerMotor.backward(.7)

def turnStraight():
    steerMotor.stop()

def goForward(speed):
    global driveStatus
    speedVar = int(speed)/100
    if driveStatus < 0:
        driveMotor.stop()
        sleep(0.2)
    driveMotor.forward(speedVar)
    driveStatus = speedVar

def goBackward(speed):
    global driveStatus
    speedVar = int(speed)/100
    if driveStatus > 0:
        driveMotor.stop()
        sleep(0.2)
    driveMotor.backward(speedVar)
    driveStatus = speedVar*-1

def stop():
    global driveStatus
    driveMotor.stop()
    driveStatus = 0